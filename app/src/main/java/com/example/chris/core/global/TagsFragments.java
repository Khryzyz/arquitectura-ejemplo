package com.example.chris.core.global;

public class TagsFragments {


    public static String tagFragHome = "tagFragHome";

    public static String tagFragHomePosts = "tagFragHomePosts";

    public static String tagFragHomeButton = "tagFragHomeButton";

    public static String tagFragDetailPost = "tagFragDetailPost";

    public static String tagFragDetailPostInfo = "tagFragDetailPostInfo";

    public static String tagFragDetailPostUser = "tagFragDetailPostUser";

    public static String tagFragMainWeather = "tagFragMainWeather";

}
