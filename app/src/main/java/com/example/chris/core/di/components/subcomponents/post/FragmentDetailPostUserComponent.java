package com.example.chris.core.di.components.subcomponents.post;

import com.example.chris.core.di.modules.sections.post.FragmentDetailPostInfoModule;
import com.example.chris.core.di.modules.sections.post.FragmentDetailPostUserModule;
import com.example.chris.ui.detailPost.fragments.info.FragmentDetailPostInfoView;
import com.example.chris.ui.detailPost.fragments.usuario.FragmentDetailPostUserView;

import dagger.Subcomponent;

/**
 * Subcomponente de la app para inyeccion al FragmentDetailPostInfoModule
 */
@Subcomponent(modules = {FragmentDetailPostUserModule.class})
public interface FragmentDetailPostUserComponent {

    /**
     * Inyeccion a la instancia del fragmento view
     *
     * @param fragmentDetailPostUserView
     */
    void inject(FragmentDetailPostUserView fragmentDetailPostUserView);

}
