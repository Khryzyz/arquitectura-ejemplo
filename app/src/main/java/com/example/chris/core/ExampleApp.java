package com.example.chris.core;

import android.app.Application;

import com.example.chris.core.di.components.AppComponent;
import com.example.chris.core.di.components.DaggerAppComponent;
import com.facebook.stetho.Stetho;

import net.danlew.android.joda.JodaTimeAndroid;

public class ExampleApp extends Application {

    /**
     * Variables
     */
    private static AppComponent appComponent;

    private static ExampleApp exampleApp;

    /**
     * Getters
     */
    public static AppComponent getAppComponent() {

        //Instancia de las inyecciones
        return appComponent;
    }

    public static ExampleApp getExampleApp() {

        //Instancia del contexto de la aplicacion
        return exampleApp;
    }

    /**
     * Sobrecarga del metodo on Create
     */
    @Override
    public void onCreate() {

        super.onCreate();

        //Inyecta las dependencias de forma general
        injectDependencies();

        Stetho.initializeWithDefaults(this);

        JodaTimeAndroid.init(this);

        //Obtiene el contexto de la aplicacion
        exampleApp = this;

    }

    /**
     * Metodo para inyectar dependencias
     */
    private void injectDependencies() {

        //Constructor del componente general
        appComponent = DaggerAppComponent
                .builder()
                .build();

        //Inyecta el contexto de la app
        appComponent.inject(this);

    }

}
