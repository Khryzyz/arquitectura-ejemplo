package com.example.chris.core.utils;

/**
 * Clase utilitaria para gestion de fechas
 */
public class DateUtils {

    /**
     * Metodo para obtener el dia de la semana a partir de un numero
     *
     * @param weekday
     * @return
     */
    public static String weekdayToText(int weekday) {

        switch (weekday) {

            case 1:
                return "Lunes";
            case 2:
                return "Martes";
            case 3:
                return "Miercoles";
            case 4:
                return "Jueves";
            case 5:
                return "Viernes";
            case 6:
                return "Sabado";
            case 7:
                return "Domingo";
            default:
                return "Dia desconocido";

        }

    }

    /**
     * Metodo para obtener el mes del año a partir de un numero
     *
     * @param month
     * @return
     */
    public static String monthToText(int month) {

        switch (month) {

            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Ocvtubre";
            case 11:
                return "Noviembre";
            case 12:
                return "Diciembre";
            default:
                return "Mes desconocido";

        }

    }

}
