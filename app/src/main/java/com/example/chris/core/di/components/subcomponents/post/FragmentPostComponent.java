package com.example.chris.core.di.components.subcomponents.post;

import com.example.chris.core.di.modules.sections.post.FragmentPostModule;
import com.example.chris.ui.home.fragments.post.FragmentPostView;

import dagger.Subcomponent;

/**
 * Subcomponente de la app para inyeccion al FragmentPostModule
 */
@Subcomponent(modules = {FragmentPostModule.class})
public interface FragmentPostComponent {

    /**
     * Inyeccion a la instancia del fragmento view
     *
     * @param fragmentPostView
     */
    void inject(FragmentPostView fragmentPostView);

}
