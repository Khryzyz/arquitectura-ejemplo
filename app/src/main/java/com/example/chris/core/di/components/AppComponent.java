package com.example.chris.core.di.components;

import com.example.chris.core.ExampleApp;
import com.example.chris.core.di.components.subcomponents.clima.FragmentLocationComponent;
import com.example.chris.core.di.components.subcomponents.clima.FragmentWeatherComponent;
import com.example.chris.core.di.components.subcomponents.clima.FragmentWindComponent;
import com.example.chris.core.di.components.subcomponents.post.FragmentDetailPostInfoComponent;
import com.example.chris.core.di.components.subcomponents.post.FragmentDetailPostUserComponent;
import com.example.chris.core.di.components.subcomponents.post.FragmentPostComponent;
import com.example.chris.core.di.modules.AppModule;
import com.example.chris.core.di.modules.repositories.PostRepositoryModule;
import com.example.chris.core.di.modules.repositories.UserRepositoryModule;
import com.example.chris.core.di.modules.repositories.WeatherRepositoryModule;
import com.example.chris.core.di.modules.sections.clima.FragmentLocationModule;
import com.example.chris.core.di.modules.sections.clima.FragmentWeatherModule;
import com.example.chris.core.di.modules.sections.clima.FragmentWindModule;
import com.example.chris.core.di.modules.sections.post.FragmentDetailPostInfoModule;
import com.example.chris.core.di.modules.sections.post.FragmentDetailPostUserModule;
import com.example.chris.core.di.modules.sections.post.FragmentPostModule;
import com.example.chris.core.di.modules.services.clima.WeatherDatabaseModule;
import com.example.chris.core.di.modules.services.clima.WeatherNetworkingModule;
import com.example.chris.core.di.modules.services.post.PostDatabaseModule;
import com.example.chris.core.di.modules.services.post.PostNetworkingModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Componente general de la app
 * Modulos:
 * - Modulo General de la App
 * - Modulo de servicio para consumo de webservices
 * - Modulo de servicio para gestion de base de datos
 * - Modulo de repositorio para el WeatherRepository
 */
@Component(modules = {
        AppModule.class,

        //Modulos de consumos de servicios web
        WeatherNetworkingModule.class,
        PostNetworkingModule.class,

        //Modulos de consumos de servicios de persistencia
        WeatherDatabaseModule.class,
        PostDatabaseModule.class,

        //Modulos de manejo de repositorio
        WeatherRepositoryModule.class,
        PostRepositoryModule.class,
        UserRepositoryModule.class,
})
@Singleton
public interface AppComponent {

    /**
     * Inyeccion a la instancia principal de la App
     *
     * @param exampleApp
     */
    void inject(ExampleApp exampleApp);

    /*#############################################################################################
    Metodos para inyeccion de secciones
    #############################################################################################*/
    //Componentes del clima
    FragmentWeatherComponent agregarModulo(FragmentWeatherModule module);

    FragmentLocationComponent agregarModulo(FragmentLocationModule module);

    FragmentWindComponent agregarModulo(FragmentWindModule module);

    //Componentes del post
    FragmentPostComponent agregarModulo(FragmentPostModule module);

    FragmentDetailPostInfoComponent agregarModulo(FragmentDetailPostInfoModule module);

    FragmentDetailPostUserComponent agregarModulo(FragmentDetailPostUserModule module);

}
