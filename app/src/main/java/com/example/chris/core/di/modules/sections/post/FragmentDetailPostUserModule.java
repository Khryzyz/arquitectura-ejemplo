package com.example.chris.core.di.modules.sections.post;

import com.example.chris.ui.detailPost.fragments.usuario.FragmentDetailPostUser;
import com.example.chris.ui.detailPost.fragments.usuario.FragmentDetailPostUserPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo de fragmento para el view
 * Provee:
 * - Vista del fragmento
 * - Presentador del fragmento
 */
@Module
public class FragmentDetailPostUserModule {

    //Fragmento principal de la seccion inicia la inyeccion en cascada
    private FragmentDetailPostUser.view view;

    /**
     * Constructor del modulo
     *
     * @param view Vista del modulo
     */
    public FragmentDetailPostUserModule(FragmentDetailPostUser.view view) {

        this.view = view;

    }

    /**
     * Proveedor de la Vista del fragmento
     *
     * @return view
     */
    @Provides
    public FragmentDetailPostUser.view providesView() {

        return view;

    }

    /**
     * Proveedor del presentador del fragmento
     *
     * @param presenter
     * @return presenter
     */
    @Provides
    public FragmentDetailPostUser.presenter providesPresenter(FragmentDetailPostUserPresenter presenter) {

        return presenter;

    }

}
