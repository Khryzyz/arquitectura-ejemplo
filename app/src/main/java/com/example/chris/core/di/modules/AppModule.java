package com.example.chris.core.di.modules;

import android.content.Context;

import com.example.chris.core.ExampleApp;
import com.example.chris.gestion.services.session.SessionService;
import com.example.chris.gestion.services.session.StorageSessionServiceImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo general de la app.
 * provee:
 * - Contexto de la app
 * - Servicio de sesion
 */
@Module
public class AppModule {

    /**
     * Proveedor del contexto de la app
     *
     * @return
     */
    @Provides
    public Context providesContext() {

        return ExampleApp.getExampleApp();
    }

    /**
     * Proveedor del servicio de sesion
     *
     * @param storageSessionService
     * @return
     */
    @Provides
    public SessionService providesSessionService(StorageSessionServiceImpl storageSessionService) {

        return storageSessionService;
    }

}
