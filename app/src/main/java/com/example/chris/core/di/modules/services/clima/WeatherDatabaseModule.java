package com.example.chris.core.di.modules.services.clima;

import android.content.Context;

import androidx.room.Room;

import com.example.chris.gestion.services.database.schemes.WeatherDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo de servicio para persistencia en base de datos
 * Provee:
 * - Constructor de la base de datos con la libreria ROOM
 */
@Module
public class WeatherDatabaseModule {

    /**
     * Proveedor del contructor de la base de datos
     *
     * @param context
     * @return
     */
    @Provides
    @Singleton
    public WeatherDatabase providesDatabase(Context context) {

        WeatherDatabase weatherDatabase = Room.inMemoryDatabaseBuilder(context, WeatherDatabase.class)
                .fallbackToDestructiveMigration()
                .build();
        return weatherDatabase;
    }

}
