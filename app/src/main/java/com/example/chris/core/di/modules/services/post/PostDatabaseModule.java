package com.example.chris.core.di.modules.services.post;

import android.content.Context;

import androidx.room.Room;

import com.example.chris.gestion.services.database.schemes.PostDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo de servicio para persistencia en base de datos
 * Provee:
 * - Constructor de la base de datos con la libreria ROOM
 */
@Module
public class PostDatabaseModule {

    /**
     * Proveedor del contructor de la base de datos
     *
     * @param context
     * @return
     */
    @Provides
    @Singleton
    public PostDatabase providesDatabase(Context context) {

        PostDatabase postDatabase = Room.inMemoryDatabaseBuilder(context, PostDatabase.class)
                .fallbackToDestructiveMigration()
                .build();

        return postDatabase;
    }

}
