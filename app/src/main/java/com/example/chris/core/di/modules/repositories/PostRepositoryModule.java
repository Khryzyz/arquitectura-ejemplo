package com.example.chris.core.di.modules.repositories;

import com.example.chris.gestion.interactors.database.PostDatabaseInteractor;
import com.example.chris.gestion.interactors.database.PostDatabaseInteractorImpl;
import com.example.chris.gestion.interactors.memory.PostMemoryInteractor;
import com.example.chris.gestion.interactors.memory.PostMemoryInteractorImpl;
import com.example.chris.gestion.interactors.network.PostNetworkInteractor;
import com.example.chris.gestion.interactors.network.PostNetworkInteractorImpl;
import com.example.chris.gestion.repositories.PostRepository;
import com.example.chris.gestion.repositories.PostRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo de repositorio para el PostRepository
 * Provee:
 * - Repositorio con la implementacion del interactuadores y una sessionService
 * - Interactuador para el consumo del WebService
 * - Interactuador para la gestion de la base de datos
 * - Interactuador para el acceso a la memoria??
 */
@Module
public class PostRepositoryModule {

    /**
     * Proveedor del interactuador para el consumo del web service
     *
     * @param postNetworkInteractor
     * @return
     */
    @Provides
    @Singleton
    public PostNetworkInteractor providePostNetwork(PostNetworkInteractorImpl postNetworkInteractor) {

        return postNetworkInteractor;
    }

    /**
     * Proveedor del inetractuador para el consumo de memoria
     *
     * @param postMemoryInteractor
     * @return
     */
    @Provides
    @Singleton
    public PostMemoryInteractor provideMemoryInteractor(PostMemoryInteractorImpl postMemoryInteractor) {

        return postMemoryInteractor;
    }


    /**
     * Proveedor del interactuador para la gestion de la base de datos
     *
     * @param postDatabaseInteractor
     * @return
     */
    @Provides
    public PostDatabaseInteractor providePostDatabase(PostDatabaseInteractorImpl postDatabaseInteractor) {

        return postDatabaseInteractor;
    }

    /**
     * Proveedor del repositorio con la implementacion del interactuadores y una sessionService
     *
     * @param postNetworkInteractor
     * @return
     */
    @Provides
    @Singleton
    public PostRepository provideHomeRepository(PostNetworkInteractor postNetworkInteractor,
                                                PostMemoryInteractor postMemoryInteractor,
                                                PostDatabaseInteractor postDatabaseInteractor) {

        //Retorno de la nueva instancia del repositorio con los interactuadores requeridos
        return new PostRepositoryImpl(
                postNetworkInteractor,
                postMemoryInteractor,
                postDatabaseInteractor
        );

    }

}
