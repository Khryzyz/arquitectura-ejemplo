package com.example.chris.core.di.components.subcomponents.post;

import com.example.chris.core.di.modules.sections.post.FragmentDetailPostInfoModule;
import com.example.chris.ui.detailPost.fragments.info.FragmentDetailPostInfoView;

import dagger.Subcomponent;

/**
 * Subcomponente de la app para inyeccion al FragmentDetailPostInfoModule
 */
@Subcomponent(modules = {FragmentDetailPostInfoModule.class})
public interface FragmentDetailPostInfoComponent {

    /**
     * Inyeccion a la instancia del fragmento view
     *
     * @param fragmentDetailPostInfoView
     */
    void inject(FragmentDetailPostInfoView fragmentDetailPostInfoView);

}
