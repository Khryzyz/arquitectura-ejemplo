package com.example.chris.core.di.modules.repositories;

import com.example.chris.gestion.interactors.network.UserNetworkInteractor;
import com.example.chris.gestion.interactors.network.UserNetworkInteractorImpl;
import com.example.chris.gestion.repositories.UserRepository;
import com.example.chris.gestion.repositories.UserRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo de repositorio para el UserRepository
 * Provee:
 * - Repositorio con la implementacion del interactuadores y una sessionService
 * - Interactuador para el consumo del WebService
 */
@Module
public class UserRepositoryModule {

    /**
     * Proveedor del interactuador para el consumo del web service
     *
     * @param userNetworkInteractor
     * @return
     */
    @Provides
    @Singleton
    public UserNetworkInteractor provideUserNetwork(UserNetworkInteractorImpl userNetworkInteractor) {

        return userNetworkInteractor;
    }

    /**
     * Proveedor del repositorio con la implementacion del interactuadores y una sessionService
     *
     * @param userNetworkInteractor
     * @return
     */
    @Provides
    @Singleton
    public UserRepository provideHomeRepository(UserNetworkInteractor userNetworkInteractor) {

        //Retorno de la nueva instancia del repositorio con los interactuadores requeridos
        return new UserRepositoryImpl(
                userNetworkInteractor
        );

    }

}
