package com.example.chris.core.base;

import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.chris.R;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Base de las actividades de tipo InterfacePresenter
 *
 * @param <T>
 */
public class BaseActivityGeneral<T extends InterfacePresenter>
        extends BaseActivityHome {

    //TAG de identificacion
    public static String TAG = BaseActivityGeneral.class.getSimpleName();

    //Controles
    @BindView(R.id.clyGeneral)
    CoordinatorLayout coordinatorLayoutGeneral;

    //Inyeccion del presentador
    @Inject
    T presenter;

    /*#############################################################################################
    Metodos sobrecargados de la clase
    #############################################################################################*/

    /**
     * Metodo sobrecargado que recibe la vista a configurar
     *
     * @param layoutResID Identificador de la vista
     */
    @Override
    public void setContentView(int layoutResID) {

        super.setContentView(layoutResID);

        coordinatorLayout = coordinatorLayoutGeneral;

    }

    /*#############################################################################################
    Metodos de inicializacion de vistas
    #############################################################################################*/

    /**
     * Inicializa el ToolBar
     */
    @Override
    public void setupToolbar() {

        setSupportActionBar(toolbarGeneral);

        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {

            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

            actionBar.setDisplayHomeAsUpEnabled(true);

            actionBar.setDisplayShowTitleEnabled(true);

        }

    }

    /**
     * Sobrecarga de metodo del sistema que se intercepta
     * para abrir el menu de navegacion desplegable
     *
     * @param menuItem Recibe un parametro de tipo MenuItem
     *                 Para abrir el Drawer
     * @return Regresa un Boolean al realizar la acción
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case android.R.id.home:

                onBackPressed();

                return true;

        }

        return super.onOptionsItemSelected(menuItem);
    }

    /**
     * Sobrecarga el metodo del sistema que se intercepta
     * al presionar el boton atras
     */
    @Override
    public void onBackPressed() {

        super.returnView();

    }

    /*#############################################################################################
    Implementacion de la interface InterfacePresenter
    Metodos para finalizacion de vista
    #############################################################################################*/

    /**
     * Detiene el bus de eventos
     */
    @Override
    public void onStop() {

        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

}
