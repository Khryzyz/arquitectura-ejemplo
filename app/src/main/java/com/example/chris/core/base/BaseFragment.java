package com.example.chris.core.base;

import android.util.Log;

import androidx.fragment.app.Fragment;

import java.util.Objects;

import javax.inject.Inject;

/**
 * Base de los fragmentos de Tipo InterfacePresenter
 *
 * @param <T>
 */
public class BaseFragment<T extends InterfacePresenter>
        extends Fragment
        implements InterfaceView,
        InterfaceFragment {

    //Inyeccion del presentador
    @Inject
    public T presenter;

    /*#############################################################################################
    Implementacion de la interface InterfaceView
    Metodos para manejo de barra de progreso
    #############################################################################################*/

    /**
     * Metodo para mostrar la barra de progreso
     *
     * @param show
     */
    public void toggleLoading(boolean show) {

        ((BaseActivityHome) Objects.requireNonNull(getActivity())).toggleLoading(show);

    }

    /*#############################################################################################
    Implementacion de la interface InterfaceView
    Metodos para manejo de visualizacion de mensajes
    #############################################################################################*/

    /**
     * Metodo sobrecargado de InterfaceView
     * Muestra un mensaje de error
     */
    @Override
    public void showErrorMessage(String errorMessage) {

        ((BaseActivityHome) Objects.requireNonNull(getActivity())).showErrorMessage(errorMessage);

    }

    /**
     * Metodo sobrecargado de InterfaceView
     * Muestra un mensaje informativo
     */
    @Override
    public void showMessage(String message) {

        ((BaseActivityHome) Objects.requireNonNull(getActivity())).showMessage(message);

    }

    /*#############################################################################################
    Implementacion de la interface InterfaceFragment
    Metodos adicionales de la clase
    #############################################################################################*/

    /**
     * Coloca el titulo en el toolbar de la vista
     *
     * @param title titulo de la vista
     */
    @Override
    public void setTitle(String title) {

        Objects.requireNonNull(((BaseActivityHome) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(title);
    }

    /**
     * Metodo para navegar al home
     */
    @Override
    public void navigateHome() {

        ((BaseActivityHome) Objects.requireNonNull(getActivity())).navigateHome();

    }

    /**
     * Metodo para regresar en la vista del historial
     */
    @Override
    public void navigateBack() {

        try {

            Objects.requireNonNull(getActivity()).onBackPressed();

        } catch (Exception e) {

            Log.e("Exception ", e.getMessage());

            ((BaseActivityHome) Objects.requireNonNull(getActivity())).navigateHome();
        }

    }

    /*#############################################################################################
    Implementacion de la interface InterfacePresenter
    Metodos para finalizacion de vista
    #############################################################################################*/

    /**
     * Metodo sobrecargado de InterfacePresenter
     * Detiene el bus de eventos
     */
    @Override
    public void onStop() {

        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

}
