package com.example.chris.core.di.components.subcomponents.clima;

import com.example.chris.core.di.modules.sections.clima.FragmentWindModule;
import com.example.chris.ui.clima.fragments.wind.FragmentWind;

import dagger.Subcomponent;

/**
 * Subcomponente de la app para inyeccion al FragmentWindModule
 */
@Subcomponent(modules = {FragmentWindModule.class})
public interface FragmentWindComponent {

    /**
     * Inyeccion a la instancia del fragmento FragmentWind
     *
     * @param fragmentWind
     */
    void inject(FragmentWind fragmentWind);

}
