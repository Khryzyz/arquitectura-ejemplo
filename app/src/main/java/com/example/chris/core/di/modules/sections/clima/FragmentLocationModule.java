package com.example.chris.core.di.modules.sections.clima;

import com.example.chris.ui.clima.fragments.location.FragmentLocationPresenter;
import com.example.chris.ui.clima.fragments.location.FragmentLocationPresenterImpl;
import com.example.chris.ui.clima.fragments.location.FragmentLocationView;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo de fragmento para el FragmentLocation
 * Provee:
 * - Vista del fragmento
 * - Presentador del fragmento
 */
@Module
public class FragmentLocationModule {

    //Variables
    FragmentLocationView fragmentLocationView;

    /**
     * Constructor del modulo
     *
     * @param fragmentLocationView
     */
    public FragmentLocationModule(FragmentLocationView fragmentLocationView) {

        this.fragmentLocationView = fragmentLocationView;
    }

    /**
     * Proveedor de la Vista del fragmento
     *
     * @return
     */
    @Provides
    public FragmentLocationView providesView() {

        return fragmentLocationView;
    }

    /**
     * Proveedor del presentador del fragmento
     *
     * @param fragmentLocationPresenter
     * @return
     */
    @Provides
    public FragmentLocationPresenter providesPresenter(FragmentLocationPresenterImpl fragmentLocationPresenter) {

        return fragmentLocationPresenter;
    }

}
