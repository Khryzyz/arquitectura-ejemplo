package com.example.chris.core.di.components.subcomponents.clima;

import com.example.chris.core.di.modules.sections.clima.FragmentWeatherModule;
import com.example.chris.ui.clima.fragments.weather.FragmentWeatherView;

import dagger.Subcomponent;

/**
 * Subcomponente de la app para inyeccion al FragmentWeatherView
 */
@Subcomponent(modules = {FragmentWeatherModule.class})
public interface FragmentWeatherComponent {

    /**
     * Inyeccion a la instancia del fragmento FragmentWeatherView
     *
     * @param fragmentWeatherView
     */
    void inject(FragmentWeatherView fragmentWeatherView);

}
