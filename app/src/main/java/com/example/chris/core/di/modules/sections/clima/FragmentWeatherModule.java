package com.example.chris.core.di.modules.sections.clima;

import com.example.chris.ui.clima.fragments.weather.FragmentWeather;
import com.example.chris.ui.clima.fragments.weather.FragmentWeatherPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo de fragmento para el FragmentWeatherView
 * Provee:
 * - Vista del fragmento
 * - Presentador del fragmento
 */
@Module
public class FragmentWeatherModule {

    //Variables
    FragmentWeather.view view;

    /**
     * Constructor del modulo
     *
     * @param view
     */
    public FragmentWeatherModule(FragmentWeather.view view) {

        this.view = view;
    }

    /**
     * Proveedor de la Vista del fragmento
     *
     * @return
     */
    @Provides
    public FragmentWeather.view providesView() {

        return view;
    }

    /**
     * Proveedor del presentador del fragmento
     *
     * @param presenter
     * @return presenter
     */
    @Provides
    public FragmentWeather.presenter providesPresenter(FragmentWeatherPresenter presenter) {

        return presenter;
    }

}
