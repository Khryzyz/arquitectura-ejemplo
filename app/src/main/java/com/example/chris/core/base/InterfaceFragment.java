package com.example.chris.core.base;

public interface InterfaceFragment {

    void setTitle(String title);

    void navigateHome();

    void navigateBack();

}
