package com.example.chris.core.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.chris.R;
import com.example.chris.ui.home.HomeActivity;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Base de las actividades de tipo InterfacePresenter
 *
 * @param <T>
 */
public class BaseActivityHome<T extends InterfacePresenter>
        extends AppCompatActivity
        implements InterfaceView,
        InterfaceActivity {

    //TAG de identificacion
    public static String TAG = BaseActivityHome.class.getSimpleName();

    //Controles del container Home
    @Nullable
    @BindView(R.id.clyHome)
    public CoordinatorLayout coordinatorLayout;

    @Nullable
    @BindView(R.id.navHome)
    public NavigationView navigationViewHome;

    @Nullable
    @BindView(R.id.drwHome)
    public DrawerLayout drawerLayoutHome;

    //Controles del container General
    @Nullable
    @BindView(R.id.tlbGeneral)
    public Toolbar toolbarGeneral;

//    //Controles del FAB
//    @Nullable
//    @BindView(R.id.rlyGeneralFAB)
//    public RelativeLayout rlyGeneralFAB;

    //ProgressDialog
    public ProgressDialog progressDialog;

    //Inyeccion del presentador
    @Inject
    T presenter;

    //Variables
    private boolean doubleBackToExitPressedOnce = false;

    /*#############################################################################################
    Metodos sobrecargados de la clase
    #############################################################################################*/

    /**
     * Metodo sobre cargado de la clase que se ejecuta en la creacion de la instancia de la clase
     *
     * @param savedInstanceState instancia creada
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Llamado a metodo padre
        super.onCreate(savedInstanceState);

        //Compatibilidad de vectores en versiones antiguas de Android
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        //Configuración de la orientacion a portaretrato
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Inicializa la barra de progreso
        setupProgressBar();

        //Lamada al metodo que inicializa la barra de herramientas
        setupToolbar();

        //Llamada al metodo que inicializa la vista
        setupUI();

    }

    /**
     * Metodo sobrecargado que recibe la vista a configurar
     *
     * @param layoutResID Identificador de la vista
     */
    @Override
    public void setContentView(int layoutResID) {

        super.setContentView(layoutResID);

        ButterKnife.bind(this);

    }

    /*#############################################################################################
    Metodos de inicializacion de vistas
    #############################################################################################*/

    /**
     * Inicializa el Menu de navegacion desplegable
     */
    public void setupDrawerContent() {

        Objects.requireNonNull(navigationViewHome).setNavigationItemSelectedListener(

                menuItem -> {

                    uncheckAllMenuItems(navigationViewHome);

                    menuItem.setChecked(true);

                    selectItem(menuItem);

                    Objects.requireNonNull(drawerLayoutHome).closeDrawers();

                    return true;
                }
        );
    }

    /**
     * Metodo vacio para la selección de item
     *
     * @param menuItem Item del menu
     */
    public void selectItem(MenuItem menuItem) {/*...*/}


    /**
     * Remueve el check de todos los items del menu
     *
     * @param navigationView Recibe un parametro de tipo NavigationView
     *                       para quitar el check a todos los items del menu
     */
    public void uncheckAllMenuItems(NavigationView navigationView) {

        final Menu menu = navigationView.getMenu();

        for (int i = 0; i < menu.size(); i++) {

            MenuItem item = menu.getItem(i);

            if (item.hasSubMenu()) {

                SubMenu subMenu = item.getSubMenu();

                for (int j = 0; j < subMenu.size(); j++) {

                    MenuItem subMenuItem = subMenu.getItem(j);

                    subMenuItem.setChecked(false);

                }

            } else {

                item.setChecked(false);

            }
        }
    }

    /**
     * Inicializa el ToolBar
     */
    public void setupToolbar() {

        setSupportActionBar(toolbarGeneral);

        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {

            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);

            actionBar.setDisplayHomeAsUpEnabled(true);

            actionBar.setDisplayShowTitleEnabled(false);

            try {

                Objects.requireNonNull(toolbarGeneral).addView(LayoutInflater.from(this).inflate(R.layout.ui_toolbar_logo, null));

            } catch (Exception e) {

                Objects.requireNonNull(toolbarGeneral).setTitle(getResources().getString(R.string.app_name));

            }

        }
    }

    /**
     * Sobrecarga el metodo del sistema que se intercepta
     * al presionar el boton atras
     */
    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {

            super.onBackPressed();

            return;

        }

        this.doubleBackToExitPressedOnce = true;

        showMessage(getResources().getString(R.string.message_exit_app));

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);

    }

    /**
     * Metodo para regresar a vista anterior
     */
    public void returnView() {

        super.onBackPressed();

    }

    /**
     * Inicializa la barra de progreso
     */
    public void setupProgressBar() {

        progressDialog = new ProgressDialog(this);

        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDialog.setTitle(getResources().getString(R.string.text_loading));

    }

    /*#############################################################################################
   Implementacion de la interface InterfaceActivity
   Metodos para inicializacion de vistas
   #############################################################################################*/

    /**
     * Metodo vacio para la inicializacion de vistas
     */
    @Override
    public void setupUI() {/*...*/}

    /*#############################################################################################
    Implementacion de la interface InterfaceView
    Metodos para manejo de barra de progreso
    #############################################################################################*/

    /**
     * Metodo para mostrar la barra de progreso
     *
     * @param show
     */
    public void toggleLoading(boolean show) {

        if (show)

            progressDialog.show();

        else
            progressDialog.dismiss();

    }

    /*#############################################################################################
    Implementacion de la interface InterfaceView
    Metodos para manejo de visualizacion de mensajes
    #############################################################################################*/

    /**
     * Muestra un mensaje de error
     */
    @Override
    public void showErrorMessage(String errorMessage) {

        String formatMessage = String.format(getString(R.string.message_error), errorMessage);

        showMessage(formatMessage);

    }

    /**
     * Muestra un mensaje informativo
     */
    @Override
    public void showMessage(String message) {

        if (coordinatorLayout != null) {

            Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG);

            TextView textView = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);

            textView.setMaxLines(5);

            snackbar.show();

        } else

            Log.e(TAG, "Error en coordinatorLayout, mensaje a mostrar: " + message);
    }

    /*#############################################################################################
    Metodos adicionales de la clase
    #############################################################################################*/

    /**
     * Navega al Home
     */
    public void navigateHome() {

        Intent intent = new Intent(this, HomeActivity.class);

        //Agregadas banderas para no retorno
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);

    }


    /*#############################################################################################
    Implementacion de la interface InterfacePresenter
    Metodos para finalizacion de vista
    #############################################################################################*/

    /**
     * Detiene el bus de eventos
     */
    @Override
    public void onStop() {

        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }


}
