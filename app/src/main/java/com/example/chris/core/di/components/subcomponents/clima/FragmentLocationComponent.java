package com.example.chris.core.di.components.subcomponents.clima;

import com.example.chris.core.di.modules.sections.clima.FragmentLocationModule;
import com.example.chris.ui.clima.fragments.location.FragmentLocation;

import dagger.Subcomponent;

/**
 * Subcomponente de la app para inyeccion al FragmentLocation
 */
@Subcomponent(modules = {FragmentLocationModule.class})
public interface FragmentLocationComponent {

    /**
     * Inyeccion a la instancia del fragmento FragmentLocation
     *
     * @param fragmentLocation
     */
    void inject(FragmentLocation fragmentLocation);

}
