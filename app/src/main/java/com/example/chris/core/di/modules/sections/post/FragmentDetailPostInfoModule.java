package com.example.chris.core.di.modules.sections.post;

import com.example.chris.ui.detailPost.fragments.info.FragmentDetailPostInfo;
import com.example.chris.ui.detailPost.fragments.info.FragmentDetailPostInfoPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo de fragmento para el view
 * Provee:
 * - Vista del fragmento
 * - Presentador del fragmento
 */
@Module
public class FragmentDetailPostInfoModule {

    //Fragmento principal de la seccion inicia la inyeccion en cascada
    private FragmentDetailPostInfo.view view;

    /**
     * Constructor del modulo
     *
     * @param view Vista del modulo
     */
    public FragmentDetailPostInfoModule(FragmentDetailPostInfo.view view) {

        this.view = view;

    }

    /**
     * Proveedor de la Vista del fragmento
     *
     * @return view
     */
    @Provides
    public FragmentDetailPostInfo.view providesView() {

        return view;

    }

    /**
     * Proveedor del presentador del fragmento
     *
     * @param presenter
     * @return presenter
     */
    @Provides
    public FragmentDetailPostInfo.presenter providesPresenter(FragmentDetailPostInfoPresenter presenter) {

        return presenter;

    }

}
