package com.example.chris.core.di.modules.services.post;

import com.example.chris.BuildConfig;
import com.example.chris.gestion.services.networking.PostApiService;
import com.example.chris.gestion.services.networking.UserApiService;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Modulo de servicio de consultas por internet
 * Provee:
 * - Constructor del cliente HTTP con la libreria OkHttpClient
 * - Constructor del consumidor de servicio web con la Libreria Retrofit
 * - Creador del enlace retrofit para consumir un servicio
 */
@Module
public class PostNetworkingModule {

    //URL Base de donde se consumira el servicio WEB
    private String BASE_URL = "https://jsonplaceholder.typicode.com/";

    /**
     * Proveedor del constructor del cliente HTTP
     *
     * @return
     */
    @Singleton
    @Provides
    @Named("PostClient")
    public OkHttpClient provideClient() {

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG)

            clientBuilder.addNetworkInterceptor(new StethoInterceptor());

        return clientBuilder.build();

    }

    /**
     * Proveedor del constructor del consumidor de servicio web
     *
     * @param client
     * @return
     */
    @Singleton
    @Provides
    @Named("PostRetrofit")
    public Retrofit provideRetrofit(@Named("PostClient") OkHttpClient client) {

        Gson gson = new GsonBuilder().create();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
    }

    /**
     * Proveedor del creador del enlace retrofit para consumir un servicio
     *
     * @param retrofit
     * @return
     */
    @Singleton
    @Provides
    public PostApiService providePostApiService(@Named("PostRetrofit") Retrofit retrofit) {

        return retrofit.create(PostApiService.class);
    }


    /**
     * Proveedor del creador del enlace retrofit para consumir un servicio
     *
     * @param retrofit
     * @return
     */
    @Singleton
    @Provides
    public UserApiService provideUserApiService(@Named("PostRetrofit") Retrofit retrofit) {

        return retrofit.create(UserApiService.class);
    }

}
