package com.example.chris.core.base;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter<V extends InterfaceView>{

    /**
     * Variables
     */
    //Maneja el bus de eventos
    public CompositeDisposable compositeDisposable = new CompositeDisposable();

    //Maneja la vista generica
    public V view;

    /**
     * Metodos para detener el bus de eventos
     */
    public void onStop() {

        compositeDisposable.clear();
    }

    /**
     * Implementacion de la interface del manejador de error
     *
     * @param throwable
     */
    public void onError(Throwable throwable) {

        view.showErrorMessage(throwable.getLocalizedMessage());
    }

}
