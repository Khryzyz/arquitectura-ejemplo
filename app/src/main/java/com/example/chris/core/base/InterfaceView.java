package com.example.chris.core.base;

public interface InterfaceView {

    void toggleLoading(boolean show);

    void showErrorMessage(String errorMessage);

    void showMessage(String message);

}
