package com.example.chris.ui.detailPost.fragments.info;

import com.example.chris.gestion.entities.Post;
import com.example.chris.gestion.repositories.PostRepository;
import com.example.chris.core.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class FragmentDetailPostInfoPresenter
        extends BasePresenter<FragmentDetailPostInfo.view>
        implements FragmentDetailPostInfo.presenter {

    @Inject
    PostRepository postRepository;

    /**
     * @param view
     * @param postRepository
     */
    @Inject
    public FragmentDetailPostInfoPresenter(FragmentDetailPostInfo.view view,
                                           PostRepository postRepository) {

        this.postRepository = postRepository;

        this.view = view;
    }

    /**
     * @param post
     */
    @Override
    public void getPost(Post post) {

        compositeDisposable.add(
                postRepository
                        .getPost(post)
                        .doOnError(this::onError)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(view::onGetPostSucces)
        );

    }

    /**
     * @param throwable
     */
    @Override
    public void onError(Throwable throwable) {

        view.onGetPostError();
    }

}
