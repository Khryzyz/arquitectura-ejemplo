package com.example.chris.ui.clima.fragments.location;

import com.example.chris.core.base.InterfaceView;
import com.example.chris.gestion.entities.WeatherData;

public interface FragmentLocationView
        extends InterfaceView {

    void showLocation(WeatherData weatherData);

}
