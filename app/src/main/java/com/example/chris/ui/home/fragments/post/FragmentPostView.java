package com.example.chris.ui.home.fragments.post;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.chris.R;
import com.example.chris.gestion.entities.Post;
import com.example.chris.core.ExampleApp;
import com.example.chris.core.base.BaseFragment;
import com.example.chris.core.controls.ItemSwipe;
import com.example.chris.core.di.modules.sections.post.FragmentPostModule;
import com.example.chris.ui.adapters.AdapterPost;
import com.example.chris.ui.home.HomeActivity;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentPostView
        extends BaseFragment<FragmentPost.presenter>
        implements FragmentPost.view,
        AdapterPost.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        ItemSwipe.RecyclerItemTouchHelperListener {

    //Controles
    @BindView(R.id.rcwRecyclerItems)
    RecyclerView rcwRecyclerItems;

    @BindView(R.id.txvRecyclerItems)
    TextView txvRecyclerItems;

    @BindView(R.id.vswRecyclerItems)
    ViewSwitcher vswRecyclerItems;

    @BindView(R.id.swpRecyclerItems)
    SwipeRefreshLayout swpRecyclerItems;

    //Recursos
    @BindString(R.string.message_eliminacion_post)
    String message_eliminacion_post;

    @BindString(R.string.message_no_post)
    String message_no_post;

    @BindString(R.string.message_error_sin_conexion)
    String message_error_sin_conexion;

    //Inyecciones
    @Inject
    AdapterPost adapterPost;

    /*#############################################################################################
    Metodo sobrecargados del sistema
    #############################################################################################*/

    /**
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        ExampleApp
                .getAppComponent()
                .agregarModulo(new FragmentPostModule(this))
                .inject(this);

    }

    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.ui_recycler_items, container, false);

        ButterKnife.bind(this, view);

        return view;

    }

    /**
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        swpRecyclerItems.setOnRefreshListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        rcwRecyclerItems.setLayoutManager(linearLayoutManager);

        rcwRecyclerItems.setAdapter(adapterPost);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemSwipe(0, ItemTouchHelper.LEFT, this));

        itemTouchHelper.attachToRecyclerView(rcwRecyclerItems);

        getPosts();

        Log.e("onViewCreated", "....");

    }

    /*#############################################################################################
    Metodos propios de la clase
    #############################################################################################*/

    /**
     * Metodo para obtener los posts
     */
    public void getPosts() {

        swpRecyclerItems.setRefreshing(true);

        presenter.getAllPosts();

        Log.e("getPosts", "....");

    }

    /**
     * Manejador de la vista para la obtencion correcta de los posts
     *
     * @param posts
     */
    private void handlerPostSuccess(List<Post> posts) {

        Log.e("handlerPostSuccess", "....");

        swpRecyclerItems.setRefreshing(false);

        adapterPost.swapData(posts);

        if (R.id.rcwRecyclerItems == vswRecyclerItems.getNextView().getId()) {

            vswRecyclerItems.showNext();

        }

    }

    /**
     * @param message
     */
    private void handlerPostError(String message) {

        swpRecyclerItems.setRefreshing(false);

        if (R.id.txvRecyclerItems == vswRecyclerItems.getNextView().getId())

            vswRecyclerItems.showNext();

        txvRecyclerItems.setText(message);

    }

    /*#############################################################################################
    Metodos sobrecargados de la vista
    #############################################################################################*/

    /**
     * Metodo que obtiene los posts
     *
     * @param post
     */
    @Override
    public void onGetAllPostsSuccess(List<Post> post) {

        handlerPostSuccess(post);

    }

    @Override
    public void onGetAllPostsError() {

        handlerPostError(message_error_sin_conexion);

    }

    /*#############################################################################################
    Metodos del escuchador de click del adaptador
    #############################################################################################*/

    /**
     * @param post Información del Post a mostrar
     */
    @Override
    public void onClickDetallePost(Post post) {

        ((HomeActivity) Objects.requireNonNull(getActivity())).navigateDetailPost(post);

    }

    /*#############################################################################################
    Metodos del escuchador de swipe del RecyclerView
    #############################################################################################*/

    /**
     * @param viewHolder
     * @param direction
     * @param position
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {

        showMessage(message_eliminacion_post);

        //Actualiza el adaptador local
        adapterPost.items.remove(position);

        adapterPost.notifyItemRemoved(position);

        //Actualiza el viewswitcher
        if (adapterPost.items.isEmpty())

            handlerPostError(message_no_post);

    }

    /*#############################################################################################
    Metodos del escuchador de refresh del RecyclerView
    #############################################################################################*/

    /**
     * Metodo sobecargado de la interface para refrescar al hacer swipe
     */
    @Override
    public void onRefresh() {


        Log.e("onRefresh", "....");

        getPosts();

    }

}
