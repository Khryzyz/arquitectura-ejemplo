package com.example.chris.ui.adapters;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.chris.R;
import com.example.chris.gestion.entities.Post;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * @author Christhian Hernando Torres - 2019
 * @version 1.0
 */
public class AdapterPost extends RecyclerView.Adapter<AdapterPost.ViewHolder> {

    public ArrayList<Post> items;

    private OnItemClickListener onItemClickListener;

    private boolean footer;

    /*#############################################################################################
    Constructor  de  la clase
    #############################################################################################*/
    public AdapterPost(OnItemClickListener onItemClickListener) {

        this.footer = true;

        this.onItemClickListener = onItemClickListener;

    }

    public AdapterPost(OnItemClickListener onItemClickListener, boolean footer) {

        this.footer = footer;

        this.onItemClickListener = onItemClickListener;

    }

    /**
     * Metodo sobre cargado del sistema que es llamado cuando se crea el Recycler View
     *
     * @param viewGroup
     * @param i
     * @return
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.item_post, viewGroup, false);

        return new ViewHolder(view);
    }

    /*#############################################################################################
    Metodo sobrecargados del sistema
    #############################################################################################*/

    /**
     * Metodo sobre cargado del sistema que es llamado cuando se enlaza la informacion con el Recycler View
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        Post post = items.get(position);

        viewHolder.flyPostHeader.setBackgroundColor(position < 20 ? viewHolder.azul : viewHolder.gris);

        viewHolder.txvPostId.setText("Id:" + post.getId());

        viewHolder.txvPostTitle.setText(post.getTitle());
//
//        if (footer) {
//
        viewHolder.imvPostIconFavorito.setImageDrawable(viewHolder.iconFavorito);

        viewHolder.imvPostIconLeido.setImageDrawable(viewHolder.iconLeido);

//            viewHolder.imvPostIconFavorito.setImageDrawable(post.isFavorito() ? viewHolder.iconFavorito : viewHolder.iconNoFavorito);
//
//            viewHolder.imvPostIconLeido.setImageDrawable(post.isLeido() ? viewHolder.iconLeido : viewHolder.iconNoLeido);
//
//        } else {
//
//            viewHolder.llyPostFooter.setVisibility(View.GONE);
//
//            viewHolder.lineFlat.setVisibility(View.GONE);
//
//        }

    }

    /**
     * Metodo sobre cargado del sistema que es llamado cuando se solicita el conteo de items
     *
     * @return
     */
    @Override
    public int getItemCount() {

        if (items != null)

            return items.size();

        return 0;

    }

    /**
     * @param post
     */
    public void swapData(List<Post> post) {

        if (post != null)

            items = new ArrayList<>(post);

        else

            items = null;

        notifyDataSetChanged();
    }

    /**
     * @param adapterPosition
     * @return
     */
    public Post obtenerPost(int adapterPosition) {

        if (items != null) {

            Post post = items.get(adapterPosition);

            if (post != null)

                return post;

        }

        return null;

    }

    /**
     * @param post
     * @return
     */
    public int getPositionItem(Post post) {

        for (int position = 0; position < items.size(); position++) {

            if (items.get(position).getId() == post.getId())

                return position;

        }

        return 0;
    }

    /**
     * Escuchador de click
     */
    public interface OnItemClickListener {

        /**
         * @param post
         */
        void onClickDetallePost(Post post);

    }

    /**
     * Un {@link RecyclerView.ViewHolder} que gestiona la vista formato del Recycler View
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        /**
         * #############################################################################################
         * Definición de variables y controles
         * #############################################################################################
         */

        @BindView(R.id.flyPostHeader)
        FrameLayout flyPostHeader;

        @BindView(R.id.txvPostId)
        TextView txvPostId;

        @BindView(R.id.txvPostTitle)
        TextView txvPostTitle;

        @BindView(R.id.imvPostIconFavorito)
        ImageView imvPostIconFavorito;

        @BindView(R.id.imvPostIconLeido)
        ImageView imvPostIconLeido;

        @BindView(R.id.lineFlat)
        FrameLayout lineFlat;

        @BindDrawable(R.drawable.ic_favorite)
        Drawable iconFavorito;

        @BindDrawable(R.drawable.ic_location)
        Drawable iconNoFavorito;

        @BindDrawable(R.drawable.ic_star)
        Drawable iconLeido;

        @BindDrawable(R.drawable.ic_location)
        Drawable iconNoLeido;

        @BindColor(R.color.colorLink)
        int azul;

        @BindColor(R.color.colorGraySecondary)
        int gris;


        /**
         * @param view
         */
        ViewHolder(View view) {

            super(view);

            ButterKnife.bind(this, view);

            view.setOnClickListener(this);

        }

        /**
         * @param view
         */
        @Override
        public void onClick(View view) {

            onItemClickListener.onClickDetallePost(
                    obtenerPost(getAdapterPosition())
            );

        }

    }

}