package com.example.chris.ui.home.fragments.post;

import com.example.chris.gestion.repositories.PostRepository;
import com.example.chris.core.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class FragmentPostPresenter
        extends BasePresenter<FragmentPost.view>
        implements FragmentPost.presenter {

    PostRepository postRepository;

    /**
     * Constructor inyectado del presentador con una instancia de la vista y el repositorio
     *
     * @param view
     * @param postRepository
     */
    @Inject
    public FragmentPostPresenter(FragmentPost.view view,
                                 PostRepository postRepository) {

        this.view = view;

        this.postRepository = postRepository;

    }

    /**
     * Llamada al metodo para obtener todos los posts
     */
    @Override
    public void getAllPosts() {

        compositeDisposable.add(
                postRepository
                        .getAllPost()
                        .doOnError(this::onError)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(view::onGetAllPostsSuccess)
        );

    }

    /**
     * @param throwable
     */
    @Override
    public void onError(Throwable throwable) {

        view.onGetAllPostsError();
    }

}
