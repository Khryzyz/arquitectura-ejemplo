package com.example.chris.ui.clima.fragments.wind;

import com.example.chris.core.base.BasePresenter;
import com.example.chris.gestion.repositories.WeatherRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class FragmentWindPresenterImpl
        extends BasePresenter<FragmentWindView>
        implements FragmentWindPresenter {

    FragmentWindView view;
    WeatherRepository weatherRepository;

    @Inject
    public FragmentWindPresenterImpl(FragmentWindView view, WeatherRepository weatherRepository) {

        this.view = view;
        this.weatherRepository = weatherRepository;
    }

    @Override
    public void onBind() {

        compositeDisposable.add(weatherRepository.getForecastData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::showWind));
    }

}
