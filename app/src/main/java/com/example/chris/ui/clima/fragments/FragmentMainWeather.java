package com.example.chris.ui.clima.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.chris.R;
import com.example.chris.ui.adapters.SectionsPagerAdapter;
import com.example.chris.ui.clima.fragments.location.FragmentLocation;
import com.example.chris.ui.clima.fragments.weather.FragmentWeatherView;
import com.example.chris.ui.clima.fragments.wind.FragmentWind;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FragmentMainWeather
        extends Fragment {

    /**
     * #############################################################################################
     * Definición de variables y controles
     * #############################################################################################
     */

    @BindView(R.id.vwpContainerTabs)
    ViewPager vwpContainerTabs;

    @BindView(R.id.tabContentTabs)
    TabLayout tabContentTabs;

    Unbinder unbinder;

    private AppBarLayout ablGeneralContainer;

    /*#############################################################################################
    Metodo sobrecargados del sistema
    #############################################################################################*/

    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.ui_viewpager_tabs, container, false);

        unbinder = ButterKnife.bind(this, view);

        ablGeneralContainer = ((View) Objects.requireNonNull(container).getParent()).findViewById(R.id.ablGeneral);

        setupViewPager(vwpContainerTabs);

        tabContentTabs.setupWithViewPager(vwpContainerTabs);

        setupTabIcons();

        return view;

    }

    /**
     * Metodo sobre cargado del sistema que es llamado cuando se destruye la vista
     */
    @Override
    public void onDestroyView() {

        if (ablGeneralContainer != null)

            ablGeneralContainer.removeView(tabContentTabs);

        super.onDestroyView();

        unbinder.unbind();

    }

    /*#############################################################################################
    Metodo propios de la clase
    #############################################################################################*/

    /**
     * Metodo que inicializa las visor de paginas del fragmento
     *
     * @param viewPager
     */
    private void setupViewPager(@NonNull ViewPager viewPager) {

        viewPager.setOffscreenPageLimit(3);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        //FragmentWeatherView

        FragmentWeatherView fragmentWeatherView = new FragmentWeatherView();

        sectionsPagerAdapter.addFragment(fragmentWeatherView, "Clima");

        //FragmentWind

        FragmentWind fragmentWind = new FragmentWind();

        sectionsPagerAdapter.addFragment(fragmentWind, "Viento");

        //FragmentLocation

        FragmentLocation fragmentLocation = new FragmentLocation();

        sectionsPagerAdapter.addFragment(fragmentLocation, "Localizacion");

        //Agregado del Adaptador

        viewPager.setAdapter(sectionsPagerAdapter);

    }


    private void setupTabIcons() {

        TextView tabClima = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.ui_tab, null);
        tabClima.setText("Clima");
        tabClima.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_moon, 0, 0);
        tabContentTabs.getTabAt(0).setCustomView(tabClima);

        TextView tabViento = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.ui_tab, null);
        tabViento.setText("Viento");
        tabViento.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_wind, 0, 0);
        tabContentTabs.getTabAt(1).setCustomView(tabViento);

        TextView tabLocalizacion = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.ui_tab, null);
        tabLocalizacion.setText("Localizacion");
        tabLocalizacion.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_location, 0, 0);
        tabContentTabs.getTabAt(2).setCustomView(tabLocalizacion);

    }

}
