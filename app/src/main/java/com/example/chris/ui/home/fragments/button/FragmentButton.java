package com.example.chris.ui.home.fragments.button;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.chris.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @author Christhian Hernando Torres - 2019
 * @version 1.0
 */
public class FragmentButton
        extends Fragment {

    Unbinder unbinder;

    /*#############################################################################################
    Constructor  de  la clase
    #############################################################################################*/
    public FragmentButton() {

    }

    /*#############################################################################################
    Metodo sobrecargados del sistema
    #############################################################################################*/

    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_button, container, false);

        unbinder = ButterKnife.bind(this, view);

        return view;

    }

    /**
     * Metodo sobre cargado del sistema que es llamado cuando se destruye la vista
     */
    @Override
    public void onDestroyView() {

        super.onDestroyView();

        unbinder.unbind();

    }

    /**
     * Metodo asignado al escuchador de click para eliminar todos los posts
     */
    @OnClick(R.id.btnHomeLimpiar)
    public void limpiarPosts() {

//        ((HomeActivity) Objects.requireNonNull(getActivity())).limpiarPosts();
    }

}