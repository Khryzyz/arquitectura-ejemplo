package com.example.chris.ui.home.fragments.post;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.chris.core.base.InterfacePresenter;
import com.example.chris.core.base.InterfaceView;
import com.example.chris.core.controls.ItemSwipe;
import com.example.chris.gestion.entities.Post;
import com.example.chris.ui.adapters.AdapterPost;

import java.util.List;

public interface FragmentPost {

    /**
     * Interface de la Vista
     * Responde a eventos de gestion de datos
     */
    interface view
            extends InterfaceView,
            AdapterPost.OnItemClickListener,
            SwipeRefreshLayout.OnRefreshListener,
            ItemSwipe.RecyclerItemTouchHelperListener {

        /**
         * Metodo que muestra los posts
         *
         * @param post
         */
        void onGetAllPostsSuccess(List<Post> post);

        /**
         * Metodo que muestra el error al obtener posts
         */
        void onGetAllPostsError();

    }

    /**
     * Interface del presentador
     */
    interface presenter
            extends InterfacePresenter {

        /**
         * Metodo que obtiene los posts
         */
        void getAllPosts();

    }

}