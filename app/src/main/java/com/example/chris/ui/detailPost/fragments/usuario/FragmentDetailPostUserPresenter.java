package com.example.chris.ui.detailPost.fragments.usuario;

import com.example.chris.gestion.entities.Post;
import com.example.chris.gestion.repositories.UserRepository;
import com.example.chris.core.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class FragmentDetailPostUserPresenter
        extends BasePresenter<FragmentDetailPostUser.view>
        implements FragmentDetailPostUser.presenter {

    @Inject
    UserRepository userRepository;

    /**
     * @param view
     * @param userRepository
     */
    @Inject
    public FragmentDetailPostUserPresenter(FragmentDetailPostUser.view view,
                                           UserRepository userRepository) {

        this.userRepository = userRepository;

        this.view = view;

    }


    /**
     * @param post
     */
    @Override
    public void getUser(Post post) {

        compositeDisposable.add(
                userRepository
                        .getUser(post)
                        .doOnError(this::onError)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(view::onGetUserSuccess)
        );

    }

    /**
     * @param throwable
     */
    @Override
    public void onError(Throwable throwable) {

        view.onGetPostError();
    }

}
