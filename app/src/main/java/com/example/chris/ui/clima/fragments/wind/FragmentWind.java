package com.example.chris.ui.clima.fragments.wind;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.chris.R;
import com.example.chris.core.ExampleApp;
import com.example.chris.core.base.BaseFragment;
import com.example.chris.core.di.modules.sections.clima.FragmentWindModule;
import com.example.chris.gestion.entities.WeatherData;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentWind
        extends BaseFragment<FragmentWindPresenter>
        implements FragmentWindView {

    @BindView(R.id.tv_wind_speed)
    TextView tvWindSpeed;

    @BindView(R.id.tv_wind_direction)
    TextView tvWindDirection;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        injectDependencies();
        presenter.onBind();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_viento, container, false);
        ButterKnife.bind(this, view);
        presenter.onBind();
        return view;
    }

    private void injectDependencies() {

        ExampleApp.getAppComponent().agregarModulo(new FragmentWindModule(this)).inject(this);
    }

    @Override
    public void showWind(WeatherData weatherData) {

        tvWindSpeed.setText(String.format(getString(R.string.wind_speed_format), weatherData.wind.speed));
        tvWindDirection.setText(String.format(getString(R.string.wind_direction_format), weatherData.wind.deg));
    }

}
