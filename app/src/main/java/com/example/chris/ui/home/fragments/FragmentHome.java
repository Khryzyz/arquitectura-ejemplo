package com.example.chris.ui.home.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.chris.R;
import com.example.chris.core.global.TagsFragments;
import com.example.chris.ui.home.fragments.button.FragmentButton;
import com.example.chris.ui.home.fragments.post.FragmentPostView;

/**
 * @author Christhian Hernando Torres - 2019
 * @version 1.0
 */
public class FragmentHome
        extends Fragment {

    /*#############################################################################################
    Constructor  de  la clase
    #############################################################################################*/
    public FragmentHome() {

    }

    /*#############################################################################################
    Metodo sobrecargados del sistema
    #############################################################################################*/

    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_home, container, false);

    }

    /**
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        FragmentManager fragmentManager = getChildFragmentManager();

        //fragmentPost
        FragmentPostView fragmentPostView = new FragmentPostView();

        fragmentPostView.setArguments(getArguments());

        fragmentManager
                .beginTransaction()
                .replace(R.id.rlyHomePosts,
                        fragmentPostView,
                        TagsFragments.tagFragHomePosts)
                .commitAllowingStateLoss();

        //FragmentButton
        FragmentButton fragmentButton = new FragmentButton();

        fragmentButton.setArguments(getArguments());

        fragmentManager
                .beginTransaction()
                .replace(R.id.rlyHomeButton,
                        fragmentButton,
                        TagsFragments.tagFragHomeButton)
                .commitAllowingStateLoss();

    }

    /*#############################################################################################
    Metodo llamados a los hijos
    #############################################################################################*/

    /**
     * Metodo llamado al fragmento hijo para limpiar los posts
     */
    public void limpiarPosts() {

//        view fragmentPost = (view) getChildFragmentManager().findFragmentByTag(TagsFragments.tagFragHomePosts);
//
//        Objects.requireNonNull(fragmentPost).limpiarPosts();

    }

    /**
     * Metodo llamado al fragmento hijo para cargar los posts
     */
    public void getPosts() {

//        view fragmentPost = (view) getChildFragmentManager().findFragmentByTag(TagsFragments.tagFragHomePosts);
//
//        Objects.requireNonNull(fragmentPost).getAllPosts();

    }

    /**
     * Metodo llamado al fragmento hijo para cambiar la vista
     *
     * @param modoVista
     */
    public void cambiarVista(int modoVista) {

//        view fragmentPost = (view) getChildFragmentManager().findFragmentByTag(TagsFragments.tagFragHomePosts);
//
//        Objects.requireNonNull(fragmentPost).cambiarVista(modoVista);

    }

}