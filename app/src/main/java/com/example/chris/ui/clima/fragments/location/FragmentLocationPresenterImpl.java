package com.example.chris.ui.clima.fragments.location;

import com.example.chris.core.base.BasePresenter;
import com.example.chris.gestion.interactors.network.WeatherNetworkInteractor;
import com.example.chris.gestion.repositories.WeatherRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 *
 */

public class FragmentLocationPresenterImpl
        extends BasePresenter<FragmentLocationView>
        implements FragmentLocationPresenter {

    FragmentLocationView fragmentLocationView;
    WeatherNetworkInteractor weatherNetworkInteractor;
    WeatherRepository weatherRepository;

    int x = 1;

    @Inject
    public FragmentLocationPresenterImpl(FragmentLocationView fragmentLocationView,
                                         WeatherRepository weatherRepository,
                                         WeatherNetworkInteractor weatherNetworkInteractor) {

        this.fragmentLocationView = fragmentLocationView;
        this.weatherRepository = weatherRepository;
        this.weatherNetworkInteractor = weatherNetworkInteractor;
    }


    @Override
    public void onBind() {

        compositeDisposable.add(
                weatherRepository.getForecastData()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(fragmentLocationView::showLocation)
        );
    }

    @Override
    public void changeLocation(String name) {

        compositeDisposable.add(weatherNetworkInteractor.getWeatherData(name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                        },
                        error -> fragmentLocationView.showErrorMessage(error.getLocalizedMessage())
                )
        );
    }

}
