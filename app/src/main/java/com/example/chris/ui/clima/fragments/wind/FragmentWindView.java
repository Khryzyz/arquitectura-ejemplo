package com.example.chris.ui.clima.fragments.wind;

import com.example.chris.core.base.InterfaceView;
import com.example.chris.gestion.entities.WeatherData;

public interface FragmentWindView
        extends InterfaceView {

    void showWind(WeatherData weatherData);

}
