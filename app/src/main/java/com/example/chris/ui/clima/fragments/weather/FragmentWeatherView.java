package com.example.chris.ui.clima.fragments.weather;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.chris.R;
import com.example.chris.core.ExampleApp;
import com.example.chris.core.base.BaseFragment;
import com.example.chris.core.di.modules.sections.clima.FragmentWeatherModule;
import com.example.chris.core.utils.DateUtils;
import com.example.chris.gestion.entities.WeatherData;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentWeatherView
        extends BaseFragment<FragmentWeather.presenter>
        implements FragmentWeather.view {

    /**
     * Controles y Variables
     */
    @BindView(R.id.tv_day_name)
    TextView tvDayName;

    @BindView(R.id.tv_date)
    TextView tvDate;

    @BindView(R.id.tv_temp)
    TextView tvTemp;

    @BindView(R.id.tv_forecast)
    TextView tvForecast;

    /**
     * Sobrecarga del metodo del sistema que es llamado cuando crea el fragmento
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        injectDependencies();
    }

    /**
     * Sobrecarga del metodo del sistema que es llamado cuando crea la vista
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_clima, container, false);

        ButterKnife.bind(this, view);

        presenter.onBind();

        return view;

    }

    /**
     * Metodo para inyectar dependencias
     */
    private void injectDependencies() {

        ExampleApp
                .getAppComponent()
                .agregarModulo(new FragmentWeatherModule(this))
                .inject(this);
    }

    /**
     * Metodo sobrecargado del presentador que consulta el clima
     *
     * @param weatherData
     */
    @Override
    public void showWeather(WeatherData weatherData) {

        tvDayName.setText(DateUtils.weekdayToText(weatherData.dt.getDayOfWeek()));

        tvDate.setText(String.format(getString(R.string.date_format), weatherData.dt.getDayOfMonth(), DateUtils.monthToText(weatherData.dt.getMonthOfYear())));

        tvTemp.setText(String.format(getString(R.string.temp_format), weatherData.temp));

        tvForecast.setText(weatherData.weather.main);

    }

}
