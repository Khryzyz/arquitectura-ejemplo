package com.example.chris.ui.clima.fragments.weather;

import com.example.chris.core.base.InterfacePresenter;
import com.example.chris.core.base.InterfaceView;
import com.example.chris.gestion.entities.WeatherData;

public interface FragmentWeather {

    interface view
            extends InterfaceView {

        void showWeather(WeatherData weatherData);

    }

    interface presenter
            extends InterfacePresenter {

        void onBind();

    }

}
