package com.example.chris.ui.detailPost.fragments.usuario;

import com.example.chris.gestion.entities.Post;
import com.example.chris.gestion.entities.User;
import com.example.chris.core.base.InterfacePresenter;
import com.example.chris.core.base.InterfaceView;

public interface FragmentDetailPostUser {

    interface view
            extends InterfaceView {

        /**
         * @param user
         */
        void onGetUserSuccess(User user);

        /**
         *
         */
        void onGetPostError();

    }

    interface presenter
            extends InterfacePresenter {

        /**
         * @param post
         */
        void getUser(Post post);

    }

}
