package com.example.chris.ui.clima.fragments.location;

import com.example.chris.core.base.InterfacePresenter;

public interface FragmentLocationPresenter
        extends InterfacePresenter {

    void onBind();

    void changeLocation(String name);

}
