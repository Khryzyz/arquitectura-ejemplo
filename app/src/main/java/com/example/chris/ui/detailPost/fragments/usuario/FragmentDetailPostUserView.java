package com.example.chris.ui.detailPost.fragments.usuario;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.chris.R;
import com.example.chris.gestion.entities.Post;
import com.example.chris.gestion.entities.User;
import com.example.chris.core.ExampleApp;
import com.example.chris.core.base.BaseFragment;
import com.example.chris.core.di.modules.sections.post.FragmentDetailPostUserModule;
import com.example.chris.core.global.KeysModels;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentDetailPostUserView
        extends BaseFragment<FragmentDetailPostUser.presenter>
        implements FragmentDetailPostUser.view {

    @BindView(R.id.txvDetallePostUsuarioId)
    TextView txvDetallePostUsuarioId;
    @BindView(R.id.txvDetallePostUsuarioUser)
    TextView txvDetallePostUsuarioUser;
    @BindView(R.id.txvDetallePostUsuarioName)
    TextView txvDetallePostUsuarioName;
    @BindView(R.id.txvDetallePostUsuarioEmail)
    TextView txvDetallePostUsuarioEmail;
    @BindView(R.id.txvDetallePostUsuarioPhone)
    TextView txvDetallePostUsuarioPhone;
    @BindView(R.id.txvDetallePostUsuarioWebsite)
    TextView txvDetallePostUsuarioWebsite;
    @BindView(R.id.txvDetallePostUsuarioStreet)
    TextView txvDetallePostUsuarioStreet;
    @BindView(R.id.txvDetallePostUsuarioSuite)
    TextView txvDetallePostUsuarioSuite;
    @BindView(R.id.txvDetallePostUsuarioCity)
    TextView txvDetallePostUsuarioCity;
    @BindView(R.id.txvDetallePostUsuarioCompanyName)
    TextView txvDetallePostUsuarioCompanyName;
    @BindView(R.id.txvDetallePostUsuarioCatchPhrase)
    TextView txvDetallePostUsuarioCatchPhrase;
    @BindView(R.id.llyDetallePostUsuarioSuccess)
    LinearLayout llyDetallePostUsuarioSuccess;
    @BindView(R.id.txvDetallePostUsuarioError)
    TextView txvDetallePostUsuarioError;
    @BindView(R.id.vswDetallePostUsuario)
    ViewSwitcher vswDetallePostUsuario;

    /*#############################################################################################
    Metodo sobrecargados del sistema
    #############################################################################################*/

    /**
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        ExampleApp
                .getAppComponent()
                .agregarModulo(new FragmentDetailPostUserModule(this))
                .inject(this);

    }


    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_detail_post_user, container, false);

        ButterKnife.bind(this, view);

        return view;

    }

    /**
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        presenter.getUser((Post) Objects.requireNonNull(getArguments()).getSerializable(KeysModels.post));

        toggleLoading(true);

    }

    /*#############################################################################################
    Metodos sobrecargados de la vista
    #############################################################################################*/

    /**
     * @param user
     */
    @Override
    public void onGetUserSuccess(User user) {

        toggleLoading(false);

        //Informacion del usuario
        txvDetallePostUsuarioId.setText(user.getId());

        txvDetallePostUsuarioUser.setText(user.getUsername());

        txvDetallePostUsuarioName.setText(user.getName());

        txvDetallePostUsuarioEmail.setText(user.getEmail());

        txvDetallePostUsuarioPhone.setText(user.getPhone());

        txvDetallePostUsuarioWebsite.setText(user.getWebsite());

        //Informacion de la direccion
        txvDetallePostUsuarioStreet.setText(user.getAddress().getStreet());

        txvDetallePostUsuarioSuite.setText(user.getAddress().getSuite());

        txvDetallePostUsuarioCity.setText(user.getAddress().getCity());

        //Informacion de la empresa
        txvDetallePostUsuarioCompanyName.setText(user.getCompany().getName());

        txvDetallePostUsuarioCatchPhrase.setText(user.getCompany().getCatchPhrase());

        //Alterna el ViewSwitcher
        if (R.id.llyDetallePostUsuarioSuccess == vswDetallePostUsuario.getNextView().getId())

            vswDetallePostUsuario.showNext();
    }

    /**
     *
     */
    @Override
    public void onGetPostError() {

        toggleLoading(false);

        //Alterna el ViewSwitcher
        if (R.id.txvDetallePostUsuarioError == vswDetallePostUsuario.getNextView().getId())

            vswDetallePostUsuario.showNext();

        txvDetallePostUsuarioError.setText(getResources().getString(R.string.message_error_user));

    }

}
