package com.example.chris.ui.detailPost.fragments.info;

import com.example.chris.gestion.entities.Post;
import com.example.chris.core.base.InterfacePresenter;
import com.example.chris.core.base.InterfaceView;

public interface FragmentDetailPostInfo {

    interface view
            extends InterfaceView {

        /**
         * @param post
         */
        void onGetPostSucces(Post post);

        /**
         *
         */
        void onGetPostError();

    }

    interface presenter
            extends InterfacePresenter {

        /**
         * @param post
         */
        void getPost(Post post);

    }

}
