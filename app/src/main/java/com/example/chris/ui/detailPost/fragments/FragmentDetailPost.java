package com.example.chris.ui.detailPost.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.chris.R;
import com.example.chris.core.global.TagsFragments;
import com.example.chris.ui.detailPost.fragments.info.FragmentDetailPostInfoView;
import com.example.chris.ui.detailPost.fragments.usuario.FragmentDetailPostUserView;

public class FragmentDetailPost
        extends Fragment {

    /*#############################################################################################
    Constructor  de  la clase
    #############################################################################################*/
    public FragmentDetailPost() {

    }

    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_detail_post, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        FragmentManager fragmentManager = getChildFragmentManager();

        //FragmentDetailPostInfoView
        FragmentDetailPostInfoView fragmentDetailPostInfoView = new FragmentDetailPostInfoView();

        fragmentDetailPostInfoView.setArguments(getArguments());

        fragmentManager
                .beginTransaction()
                .replace(R.id.rlyDetailPostInfo,
                        fragmentDetailPostInfoView,
                        TagsFragments.tagFragDetailPostInfo)
                .commitAllowingStateLoss();

        //FragmentDetailPostUserView
        FragmentDetailPostUserView fragmentDetailPostUsuarioView = new FragmentDetailPostUserView();

        fragmentDetailPostUsuarioView.setArguments(getArguments());

        fragmentManager
                .beginTransaction()
                .replace(R.id.rlyDetailPostUser,
                        fragmentDetailPostUsuarioView,
                        TagsFragments.tagFragDetailPostUser)
                .commitAllowingStateLoss();

    }

}
