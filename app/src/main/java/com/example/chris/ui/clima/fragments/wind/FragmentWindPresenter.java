package com.example.chris.ui.clima.fragments.wind;

import com.example.chris.core.base.InterfacePresenter;

public interface FragmentWindPresenter
        extends InterfacePresenter {

    void onBind();

}
