package com.example.chris.ui.detailPost.fragments.info;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.chris.R;
import com.example.chris.gestion.entities.Post;
import com.example.chris.core.ExampleApp;
import com.example.chris.core.base.BaseFragment;
import com.example.chris.core.di.modules.sections.post.FragmentDetailPostInfoModule;
import com.example.chris.core.global.KeysModels;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentDetailPostInfoView
        extends BaseFragment<FragmentDetailPostInfo.presenter>
        implements FragmentDetailPostInfo.view {

    @BindView(R.id.txvDetallePostInfoId)
    TextView txvDetallePostInfoId;

    @BindView(R.id.txvDetallePostInfoTitulo)
    TextView txvDetallePostInfoTitulo;

    @BindView(R.id.txvDetallePostInfoBody)
    TextView txvDetallePostInfoBody;

    /*#############################################################################################
    Metodo sobrecargados del sistema
    #############################################################################################*/

    /**
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        ExampleApp
                .getAppComponent()
                .agregarModulo(new FragmentDetailPostInfoModule(this))
                .inject(this);

    }


    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_detail_post_info, container, false);

        ButterKnife.bind(this, view);

        return view;

    }

    /**
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        presenter.getPost((Post) Objects.requireNonNull(getArguments()).getSerializable(KeysModels.post));

    }

    /*#############################################################################################
    Metodos sobrecargados de la vista
    #############################################################################################*/

    /**
     * @param post
     */
    @Override
    public void onGetPostSucces(Post post) {

        txvDetallePostInfoId.setText(String.valueOf(post.getId()));

        txvDetallePostInfoBody.setText(post.getBody());

        txvDetallePostInfoTitulo.setText(post.getTitle());

    }

    /**
     *
     */
    @Override
    public void onGetPostError() {

    }

}
