package com.example.chris.ui.clima.fragments.weather;

import com.example.chris.core.base.BasePresenter;
import com.example.chris.gestion.repositories.WeatherRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class FragmentWeatherPresenter
        extends BasePresenter<FragmentWeather.view>
        implements FragmentWeather.presenter {

    /**
     * Variables
     */
    WeatherRepository weatherRepository;

    /**
     * Inyeccion del presentador
     *
     * @param view
     * @param weatherRepository
     */
    @Inject
    public FragmentWeatherPresenter(FragmentWeather.view view, WeatherRepository weatherRepository) {

        this.view = view;
        this.weatherRepository = weatherRepository;
    }

    /**
     * Metodo que agrega el oservador al bus de eventos
     */
    @Override
    public void onBind() {

        compositeDisposable.add(weatherRepository.getForecastData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::showWeather));
    }

}