package com.example.chris.gestion.repositories;

import com.example.chris.core.base.BaseRepository;
import com.example.chris.gestion.entities.WeatherData;
import com.example.chris.gestion.interactors.database.WeatherDatabaseInteractor;
import com.example.chris.gestion.interactors.memory.WeatherMemoryInteractor;
import com.example.chris.gestion.interactors.network.WeatherNetworkInteractor;
import com.example.chris.gestion.services.session.SessionService;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class WeatherRepositoryImpl
        extends BaseRepository
        implements WeatherRepository {

    SessionService sessionService;

    Disposable dataProviderDisposable;

    WeatherNetworkInteractor weatherNetworkInteractor;

    WeatherDatabaseInteractor weatherDatabaseInteractor;

    WeatherMemoryInteractor weatherMemoryInteractor;

    /**
     * Inyeccion del constructor del WeatherRepository
     *
     * @param sessionService
     * @param weatherNetworkInteractor
     * @param weatherDatabaseInteractor
     * @param weatherMemoryInteractor
     */
    @Inject
    public WeatherRepositoryImpl(SessionService sessionService,
                                 WeatherNetworkInteractor weatherNetworkInteractor,
                                 WeatherDatabaseInteractor weatherDatabaseInteractor,
                                 WeatherMemoryInteractor weatherMemoryInteractor) {

        this.sessionService = sessionService;

        this.weatherNetworkInteractor = weatherNetworkInteractor;

        this.weatherDatabaseInteractor = weatherDatabaseInteractor;

        this.weatherMemoryInteractor = weatherMemoryInteractor;

    }

    /**
     * Metodo sobrecargado de la interface que con obtiene los datos del pronostico
     *
     * @return
     */
    @Override
    public Observable<WeatherData> getForecastData() {

        String currentLocationName = sessionService.getLocation();

        //Observable para el interactuador de memoria
        Observable<WeatherData> memoryObservable = weatherMemoryInteractor.getAllPost().toObservable();

        //Observable para el interactuador de base de datos
        Observable<WeatherData> databaseObservable = weatherDatabaseInteractor.getWeatherData(currentLocationName).toObservable();

        //Observable para el interactuador de web service
        Observable<WeatherData> networkObservable = weatherNetworkInteractor.getWeatherData(currentLocationName).toObservable();

        if (!isNetworkInProgress()) {
            dataProviderDisposable =
                    Observable.concat(memoryObservable, databaseObservable, networkObservable)
                            .filter(data -> data.name.equals(sessionService.getLocation()))
                            .filter(WeatherData::isDataInDate)
                            .firstElement()
                            .subscribe((boosterData) -> {
                            }, this::handleNonHttpException);
        }

        return weatherMemoryInteractor.getWeatherDataObservable();
    }

    /**
     * Metodo privado que pregunta por la red
     *
     * @return
     */
    private boolean isNetworkInProgress() {

        return dataProviderDisposable != null && !dataProviderDisposable.isDisposed();
    }

}
