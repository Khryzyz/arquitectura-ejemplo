package com.example.chris.gestion.services.session;

public interface SessionService {

    void saveLocation(String name);

    String getLocation();

}
