package com.example.chris.gestion.interactors.memory;

import com.example.chris.gestion.entities.WeatherData;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class WeatherMemoryInteractorImpl
        implements WeatherMemoryInteractor {

    BehaviorSubject<WeatherData> observable;

    WeatherData weatherData;

    /**
     * Inyeccion del constructor de la clase
     */
    @Inject
    public WeatherMemoryInteractorImpl() {

        observable = BehaviorSubject.create();

    }

    /**
     * Metodo para almacenar los datos
     *
     * @param weatherData
     */
    @Override
    public void saveData(WeatherData weatherData) {

        this.weatherData = weatherData;

        observable.onNext(weatherData);

    }

    /**
     * Metodo para obtener los datos
     *
     * @return
     */
    @Override
    public Maybe<WeatherData> getAllPost() {

        return weatherData == null ? Maybe.empty() : Maybe.just(weatherData);
    }

    /**
     * Metodo para obtener los datos del observable
     *
     * @return
     */
    @Override
    public Observable<WeatherData> getWeatherDataObservable() {

        return observable;
    }

}