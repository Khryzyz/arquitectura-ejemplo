package com.example.chris.gestion.services.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.chris.gestion.entities.Post;

import java.util.List;

import io.reactivex.Maybe;

/**
 * DAO de la base de datos para la entidad Post
 */
@Dao
public interface PostDao {

    /**
     * Consulta a la entidad Post
     *
     * @return
     */
    @Query("SELECT * FROM Post")
    Maybe<List<Post>> getAllPosts();

    /**
     * Insercion a la entidad Post
     *
     * @param post
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveData(List<Post> post);

}
