package com.example.chris.gestion.repositories;

import com.example.chris.gestion.entities.Post;
import com.example.chris.gestion.entities.User;

import io.reactivex.Observable;

public interface UserRepository {

    /**
     * Obtiene un user especifico
     *
     * @return
     */
    Observable<User> getUser(Post post);

}
