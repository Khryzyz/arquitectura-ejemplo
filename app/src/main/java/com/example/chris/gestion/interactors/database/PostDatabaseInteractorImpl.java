package com.example.chris.gestion.interactors.database;

import com.example.chris.gestion.entities.Post;
import com.example.chris.gestion.interactors.memory.PostMemoryInteractor;
import com.example.chris.gestion.services.database.schemes.PostDatabase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.schedulers.Schedulers;

public class PostDatabaseInteractorImpl
        implements PostDatabaseInteractor {

    PostDatabase postDatabase;

    PostMemoryInteractor postMemoryInteractor;

    /**
     * Inyeccion del constructor de la clase
     */
    @Inject
    public PostDatabaseInteractorImpl(PostDatabase postDatabase,
                                      PostMemoryInteractor postMemoryInteractor) {

        this.postDatabase = postDatabase;

        this.postMemoryInteractor = postMemoryInteractor;

    }

    /**
     * Metodo para almacenar los datos
     *
     * @param post
     */
    @Override
    public void saveData(List<Post> post) {

        postDatabase
                .postDao()
                .saveData(post);
    }

    /**
     * Metodo para obtener los datos
     *
     * @return
     */
    @Override
    public Maybe<List<Post>> getAllPosts() {

        return postDatabase
                .postDao()
                .getAllPosts()
                .subscribeOn(Schedulers.io());
//                .doOnSuccess(postMemoryInteractor::saveData);
    }

}