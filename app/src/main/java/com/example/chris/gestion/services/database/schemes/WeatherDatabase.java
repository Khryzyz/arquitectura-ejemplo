package com.example.chris.gestion.services.database.schemes;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.chris.gestion.entities.WeatherData;
import com.example.chris.gestion.services.database.dao.WeatherDao;
import com.example.chris.gestion.services.database.typeconverters.RoomTypeConverters;

/**
 * Base de datos
 * Entidad WeatherData
 * Convertidores de tipo RoomTypeConverters
 */
@Database(entities = {WeatherData.class}, version = 1)
@TypeConverters(RoomTypeConverters.class)
public abstract class WeatherDatabase extends RoomDatabase {

    /**
     * DAO que gestiona la base de datos
     *
     * @return
     */
    public abstract WeatherDao weatherDao();

}
