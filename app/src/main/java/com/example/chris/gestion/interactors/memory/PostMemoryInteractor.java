package com.example.chris.gestion.interactors.memory;

import com.example.chris.gestion.entities.Post;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;

public interface PostMemoryInteractor {

    /**
     * Metodo para obtener los datos del observable
     *
     * @return
     */
    Observable<List<Post>> getPostObservable();

    /**
     * Metodo para almacenar los datos
     *
     * @param post
     */
    void saveData(List<Post> post);

    /**
     * Metodo para obtener los datos
     *
     * @return
     */
    Maybe<List<Post>> getAllPost();

}
