package com.example.chris.gestion.interactors.network;

import com.example.chris.gestion.entities.Post;

import java.util.List;

import io.reactivex.Single;

public interface PostNetworkInteractor {

    /**
     * Metodo para obtener todos los datos
     *
     * @return
     */
    Single<List<Post>> getAllPosts();

    /**
     * Metodo para obtener un post especifico
     *
     * @return
     */
    Single<Post> getPost(Post post);

}
