package com.example.chris.gestion.interactors.database;

import com.example.chris.gestion.entities.WeatherData;

import io.reactivex.Maybe;

public interface WeatherDatabaseInteractor {

    /**
     * Metodo para obtener los datos
     *
     * @param name
     * @return
     */
    Maybe<WeatherData> getWeatherData(String name);

    /**
     * Metodo para almacenar los datos
     *
     * @param weatherData
     */
    void saveData(WeatherData weatherData);

}
