package com.example.chris.gestion.interactors.network;

import com.example.chris.gestion.entities.Post;
import com.example.chris.gestion.entities.User;

import io.reactivex.Single;

public interface UserNetworkInteractor {

    /**
     * Metodo para obtener un post especifico
     *
     * @return
     */
    Single<User> getUser(Post post);

}
