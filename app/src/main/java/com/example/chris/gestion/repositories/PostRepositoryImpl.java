package com.example.chris.gestion.repositories;

import com.example.chris.gestion.entities.Post;
import com.example.chris.gestion.interactors.database.PostDatabaseInteractor;
import com.example.chris.gestion.interactors.memory.PostMemoryInteractor;
import com.example.chris.gestion.interactors.network.PostNetworkInteractor;
import com.example.chris.core.base.BaseRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class PostRepositoryImpl
        extends BaseRepository
        implements PostRepository {

    Disposable dataProviderDisposable;

    PostNetworkInteractor postNetworkInteractor;

    PostMemoryInteractor postMemoryInteractor;

    PostDatabaseInteractor postDatabaseInteractor;

    /**
     * Inyeccion del constructor del PostRepository
     *
     * @param postNetworkInteractor
     */
    @Inject
    public PostRepositoryImpl(PostNetworkInteractor postNetworkInteractor,
                              PostMemoryInteractor postMemoryInteractor,
                              PostDatabaseInteractor postDatabaseInteractor) {

        this.postNetworkInteractor = postNetworkInteractor;

        this.postMemoryInteractor = postMemoryInteractor;

        this.postDatabaseInteractor = postDatabaseInteractor;

    }

    /**
     * Metodo sobrecargado de la interface que con obtiene los datos del pronostico
     *
     * @return
     */
    @Override
    public Observable<List<Post>> getAllPost() {

//        //Observable para el interactuadore de memoria
//        Observable<List<Post>> memoryObservable = postMemoryInteractor.getAllPost().toObservable();
//
//        //Observable para el interactuador de base de datos
//        Observable<List<Post>> databaseObservable = postDatabaseInteractor.getAllPosts().toObservable();

        //Observable para el interactuador de web service
        Observable<List<Post>> networkObservable = postNetworkInteractor.getAllPosts().toObservable();

        if (!isNetworkInProgress()) {

            dataProviderDisposable =
//                    Observable.concat(memoryObservable, databaseObservable, networkObservable)
                    Observable.just(networkObservable)
                            .subscribe((boosterData) -> {
                            }, this::handleNonHttpException);
        }

        return networkObservable;

    }

    /**
     * Metodo para obternet la informacion de un post especifico
     *
     * @param post
     * @return
     */
    @Override
    public Observable<Post> getPost(Post post) {

        //Observable para el interactuador de web service
        Observable<Post> networkObservable = postNetworkInteractor.getPost(post).toObservable();

        if (!isNetworkInProgress()) {

            dataProviderDisposable =
                    Observable
                            .just(networkObservable)
                            .subscribe((boosterData) -> {
                            }, this::handleNonHttpException);
        }

        return networkObservable;
    }

    /**
     * Metodo privado que pregunta por la red
     *
     * @return
     */
    private boolean isNetworkInProgress() {

        return dataProviderDisposable != null && !dataProviderDisposable.isDisposed();
    }

}
