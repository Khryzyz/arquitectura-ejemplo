package com.example.chris.gestion.interactors.network;

import com.example.chris.gestion.entities.Post;
import com.example.chris.gestion.entities.User;
import com.example.chris.gestion.services.networking.UserApiService;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Interactuador para el consumo del WebService
 */
public class UserNetworkInteractorImpl
        implements UserNetworkInteractor {

    UserApiService userApiService;

    /**
     * Inyeccion del constructor de la clase
     */
    @Inject
    public UserNetworkInteractorImpl(UserApiService userApiService) {

        this.userApiService = userApiService;

    }

    /**
     * Metodo para obtener un post especifico
     *
     * @param post
     * @return
     */
    @Override
    public Single<User> getUser(Post post) {

        return userApiService
                .getUser(post.getUserId());

    }

}