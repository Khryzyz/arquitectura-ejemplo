package com.example.chris.gestion.repositories;

import com.example.chris.gestion.entities.WeatherData;

import io.reactivex.Observable;

public interface WeatherRepository {

    Observable<WeatherData> getForecastData();

}
