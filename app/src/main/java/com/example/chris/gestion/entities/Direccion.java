package com.example.chris.gestion.entities;

import java.io.Serializable;

/**
 * @author Christhian Hernando Torres - 2019
 * @version 1.0
 */
public class Direccion implements Serializable {

    //Atributos
    private String street;
    private String suite;
    private String city;

    public Direccion() {
    }

    public Direccion(String street, String suite, String city) {
        this.street = street;
        this.suite = suite;
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuite() {
        return suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}