package com.example.chris.gestion.interactors.network;

import com.example.chris.gestion.entities.WeatherData;
import com.example.chris.gestion.interactors.database.WeatherDatabaseInteractor;
import com.example.chris.gestion.interactors.memory.WeatherMemoryInteractor;
import com.example.chris.gestion.services.networking.WeatherApiService;
import com.example.chris.gestion.services.session.SessionService;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Interactuador para el consumo del WebService
 */
public class WeatherNetworkInteractorImpl
        implements WeatherNetworkInteractor {

    WeatherApiService weatherApiService;

    SessionService sessionService;

    WeatherDatabaseInteractor weatherDatabaseInteractor;

    WeatherMemoryInteractor weatherMemoryInteractor;

    /**
     * Inyeccion del constructor de la clase
     */
    @Inject
    public WeatherNetworkInteractorImpl(WeatherApiService weatherApiService,
                                        SessionService sessionService,
                                        WeatherMemoryInteractor weatherMemoryInteractor,
                                        WeatherDatabaseInteractor weatherDatabaseInteractor) {

        this.weatherApiService = weatherApiService;

        this.sessionService = sessionService;

        this.weatherMemoryInteractor = weatherMemoryInteractor;

        this.weatherDatabaseInteractor = weatherDatabaseInteractor;

    }

    /**
     * Metodo para obtener los datos
     *
     * @param city
     * @return
     */
    @Override
    public Single<WeatherData> getWeatherData(String city) {

        return weatherApiService.getWeather(city)
                .map(WeatherData::copyFromResponse)
                .doOnSuccess(data -> sessionService.saveLocation(data.name))
                .doOnSuccess(weatherDatabaseInteractor::saveData)
                .doOnSuccess(weatherMemoryInteractor::saveData);
    }

}