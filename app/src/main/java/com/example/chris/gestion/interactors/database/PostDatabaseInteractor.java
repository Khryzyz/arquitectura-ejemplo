package com.example.chris.gestion.interactors.database;

import com.example.chris.gestion.entities.Post;

import java.util.List;

import io.reactivex.Maybe;

public interface PostDatabaseInteractor {

    /**
     * Metodo para almacenar los datos
     */
    void saveData(List<Post> post);

    /**
     * Metodo para obtener los datos
     *
     * @return
     */
    Maybe<List<Post>> getAllPosts();

}
