package com.example.chris.gestion.services.networking;

import com.example.chris.gestion.entities.User;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Interface de la API que consume el servicio
 */
public interface UserApiService {

    /**
     * Consume el recurso weather atraves del metodo GET
     *
     * @param id
     * @return
     */
    @GET("users/{id}")
    Single<User> getUser(@Path("id") int id);

}
