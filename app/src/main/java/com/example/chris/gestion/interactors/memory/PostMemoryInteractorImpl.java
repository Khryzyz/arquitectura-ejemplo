package com.example.chris.gestion.interactors.memory;

import com.example.chris.gestion.entities.Post;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class PostMemoryInteractorImpl
        implements PostMemoryInteractor {

    BehaviorSubject<List<Post>> observable;

    List<Post> post;

    /**
     * Inyeccion del constructor de la clase
     */
    @Inject
    public PostMemoryInteractorImpl() {

        observable = BehaviorSubject.create();

    }

    /**
     * Metodo para almacenar los datos
     *
     * @param post
     */
    @Override
    public void saveData(List<Post> post) {

        this.post = post;

        observable.onNext(post);

    }

    /**
     * Metodo para obtener los datos
     *
     * @return
     */
    @Override
    public Maybe<List<Post>> getAllPost() {

        return post == null ? Maybe.empty() : Maybe.just(post);
    }

    /**
     * Metodo para obtener los datos del observable
     *
     * @return
     */
    @Override
    public Observable<List<Post>> getPostObservable() {

        return observable;
    }

}