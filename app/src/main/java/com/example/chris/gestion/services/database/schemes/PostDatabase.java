package com.example.chris.gestion.services.database.schemes;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.chris.gestion.entities.Post;
import com.example.chris.gestion.services.database.dao.PostDao;

/**
 * Base de datos
 * Entidad Post
 */
@Database(entities = {Post.class}, version = 1)
public abstract class PostDatabase extends RoomDatabase {

    /**
     * DAO que gestiona la entidad de la base de datos
     *
     * @return
     */
    public abstract PostDao postDao();

}
