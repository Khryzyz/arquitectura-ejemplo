package com.example.chris.gestion.repositories;

import com.example.chris.gestion.entities.Post;

import java.util.List;

import io.reactivex.Observable;

public interface PostRepository {

    /**
     * Obtiene todos los posts
     *
     * @return
     */
    Observable<List<Post>> getAllPost();

    /**
     * Obtiene un post especifico
     *
     * @return
     */
    Observable<Post> getPost(Post post);

}
