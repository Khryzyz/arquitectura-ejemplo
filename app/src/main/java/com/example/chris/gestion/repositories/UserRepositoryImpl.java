package com.example.chris.gestion.repositories;

import com.example.chris.gestion.entities.Post;
import com.example.chris.gestion.entities.User;
import com.example.chris.gestion.interactors.network.UserNetworkInteractor;
import com.example.chris.core.base.BaseRepository;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class UserRepositoryImpl
        extends BaseRepository
        implements UserRepository {

    Disposable dataProviderDisposable;

    UserNetworkInteractor userNetworkInteractor;

    /**
     * Inyeccion del constructor del PostRepository
     *
     * @param userNetworkInteractor
     */
    @Inject
    public UserRepositoryImpl(UserNetworkInteractor userNetworkInteractor) {

        this.userNetworkInteractor = userNetworkInteractor;

    }

    /**
     * Metodo para obternet la informacion de un post especifico
     *
     * @param post
     * @return
     */
    @Override
    public Observable<User> getUser(Post post) {

        //Observable para el interactuador de web service
        Observable<User> networkObservable = userNetworkInteractor.getUser(post).toObservable();

        if (!isNetworkInProgress()) {

            dataProviderDisposable =
                    Observable
                            .just(networkObservable)
                            .subscribe((boosterData) -> {
                            }, this::handleNonHttpException);
        }

        return networkObservable;
    }

    /**
     * Metodo privado que pregunta por la red
     *
     * @return
     */
    private boolean isNetworkInProgress() {

        return dataProviderDisposable != null && !dataProviderDisposable.isDisposed();
    }

}
