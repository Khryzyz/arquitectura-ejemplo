package com.example.chris.gestion.services.networking;

import com.example.chris.gestion.entities.CityForecastResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface de la API que consume el servicio
 */
//http://api.openweathermap.org/data/2.5/
public interface WeatherApiService {

    /**
     * Consume el recurso weather aatraves del metodo GET
     *
     * @param city
     * @return
     */
    @GET("weather")
    Single<CityForecastResponse> getWeather(@Query("q") String city);

}
