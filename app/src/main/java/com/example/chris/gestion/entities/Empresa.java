package com.example.chris.gestion.entities;

import java.io.Serializable;

/**
 * @author Christhian Hernando Torres - 2019
 * @version 1.0
 */
public class Empresa implements Serializable {

    //Atributos
    private String name;
    private String catchPhrase;

    public Empresa() {
    }

    public Empresa(String name, String catchPhrase) {
        this.name = name;
        this.catchPhrase = catchPhrase;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCatchPhrase() {
        return catchPhrase;
    }

    public void setCatchPhrase(String catchPhrase) {
        this.catchPhrase = catchPhrase;
    }
}