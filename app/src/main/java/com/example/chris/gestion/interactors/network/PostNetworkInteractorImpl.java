package com.example.chris.gestion.interactors.network;

import com.example.chris.gestion.entities.Post;
import com.example.chris.gestion.interactors.database.PostDatabaseInteractor;
import com.example.chris.gestion.interactors.memory.PostMemoryInteractor;
import com.example.chris.gestion.services.networking.PostApiService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Interactuador para el consumo del WebService
 */
public class PostNetworkInteractorImpl
        implements PostNetworkInteractor {

    PostApiService postApiService;

    PostDatabaseInteractor postDatabaseInteractor;

    PostMemoryInteractor postMemoryInteractor;

    /**
     * Inyeccion del constructor de la clase
     */
    @Inject
    public PostNetworkInteractorImpl(PostApiService postApiService,
                                     PostDatabaseInteractor postDatabaseInteractor,
                                     PostMemoryInteractor postMemoryInteractor) {

        this.postApiService = postApiService;

        this.postDatabaseInteractor = postDatabaseInteractor;

        this.postMemoryInteractor = postMemoryInteractor;

    }

    /**
     * Metodo para obtener los datos
     *
     * @return
     */
    @Override
    public Single<List<Post>> getAllPosts() {

        return postApiService
                .getAllPosts()
                .doOnSuccess(postDatabaseInteractor::saveData)
                .doOnSuccess(postMemoryInteractor::saveData);

    }

    /**
     * Metodo para obtener un post especifico
     *
     * @param post
     * @return
     */
    @Override
    public Single<Post> getPost(Post post) {

        return postApiService
                .getPost(post.getId());
    }

}