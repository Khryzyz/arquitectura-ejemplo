package com.example.chris.gestion.services.networking;

import com.example.chris.gestion.entities.Post;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Interface de la API que consume el servicio
 */
public interface PostApiService {

    /**
     * Consume el recurso posts atraves del metodo GET
     * Obtiene todos los posts
     *
     * @return
     */
    @GET("posts")
    Single<List<Post>> getAllPosts();


    /**
     * Consume el recurso weather atraves del metodo GET
     *
     * @param id
     * @return
     */
    @GET("posts/{id}")
    Single<Post> getPost(@Path("id") int id);

}
