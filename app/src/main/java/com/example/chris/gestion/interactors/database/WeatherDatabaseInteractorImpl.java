package com.example.chris.gestion.interactors.database;

import com.example.chris.gestion.entities.WeatherData;
import com.example.chris.gestion.interactors.memory.WeatherMemoryInteractor;
import com.example.chris.gestion.services.database.schemes.WeatherDatabase;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.schedulers.Schedulers;

public class WeatherDatabaseInteractorImpl
        implements WeatherDatabaseInteractor {

    WeatherDatabase weatherDatabase;

    WeatherMemoryInteractor weatherMemoryInteractor;

    /**
     * Inyeccion del constructor de la clase
     */
    @Inject
    public WeatherDatabaseInteractorImpl(WeatherDatabase weatherDatabase,
                                         WeatherMemoryInteractor weatherMemoryInteractor) {

        this.weatherDatabase = weatherDatabase;

        this.weatherMemoryInteractor = weatherMemoryInteractor;

    }

    /**
     * Metodo para obtener los datos
     *
     * @param name
     * @return
     */
    @Override
    public Maybe<WeatherData> getWeatherData(String name) {

        return weatherDatabase.weatherDao()
                .getWeather(name)
                .subscribeOn(Schedulers.io())
                .doOnSuccess(weatherMemoryInteractor::saveData);
    }

    /**
     * Metodo para almacenar los datos
     *
     * @param weatherData
     */
    @Override
    public void saveData(WeatherData weatherData) {

        weatherDatabase.weatherDao()
                .saveData(weatherData);
    }

}