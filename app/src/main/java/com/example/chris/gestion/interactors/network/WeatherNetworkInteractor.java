package com.example.chris.gestion.interactors.network;

import com.example.chris.gestion.entities.WeatherData;

import io.reactivex.Single;

public interface WeatherNetworkInteractor {

    /**
     * Metodo para obtener los datos
     *
     * @param city
     * @return
     */
    Single<WeatherData> getWeatherData(String city);

}
