Si el fragmento necesita comunicarse con el repositorio:

//Inyeccion de dependencias vista presentador y repositorio

1.Crear la interface de vista del fragment que extiende la "InterfaceView"

2.Crear el fragment e implementar la vista y extender de BaseFragment tipificandolo con el presentador (aun no creado)

3. crear la interface del presentador que extiende la "InterfacePresenter"

4. crear el presentador e implementar el presentador y extiende del BasePresenter tipificandolo con la vista

5. Agregar el modulo en la carpeta 
dagger/modules/sections
 
 debe contener una instancia de la vista
 
 un constructor que reciba una instancia de la vista y la paso a local
 
 se crea proveedor de la vista
 
 se crea un proveedor del presentador
 
6. Agregar componente en el a carpeta 
dagger/subcomponent

se referencia al modulo en la cabecera

se crea el metodo inject en el fragmento

7. agrega el subcomponente a el componente app mediante el metodo "plus"

8. Se crea la interface del repositorio con un metodo para obtener datos de tipo observable por el momento vacio;

9. Se crea el repositorio y se implementa la interface

10. se crea un modulo de repositorio en donde se incluiran los interactuadores de los servicios

se proveen los interactuadores necesarios

11. Se agrega el modulo en el AppComponent

12. en el fragment home se inyectan las dependencias en plus se registra la nueva instancia del modulo psandole la vista por parametro

13. En la implementacion del presentador agregar al composite el repositorio y subsicribirlo a la vista

//Consumo de una api

- Creando el modulo de consumo

14. Creamos un modulo PostNetWorkingModule

va a proveer un cliente

a proveer una instancia de retrofit

y por ultimo una instancia de PostApiService (que no se ha creado)

15. vamos a crear el consumo de la api en services/networking creamos una interface PostApiService

16. en el repositorio implementamos la funcion getPostData que consume al interactuador de la api

17. Implementamos los proveedores del PostNetWorkInteractor en el modulo del repositorio y lo agragmos en el constructor del proveedor de repositorio

18. No olvidar agregar el PostNetrworkingMOdule al componente de la app

14. Se crea la interface del interactuador con un metodo "getPosts" de tipo Single<Post>